 
// Quand validation formulaire => doit sélectionner un chat

// Minimum 15 caractères

// Si pas de validation => bordures = rouge
// =/ vert

// Bonus :  Remplacer le formulaire par un message de confirmation si les champs sont valides. 

let raisonOk, selectOk

window.onload = function () { 

// On récupère le champs
let raison = document.getElementById("raison");
let choix = document.getElementById("choix");
let selection = document.getElementById("selection");

// label for="raison">Raison d'adoption </label>

// On écoute l'évènement
raison.addEventListener("change", verifRaison);
// choix.addEventListener("change", verifSelect);
selection.addEventListener("change", verifSelect);

} // Fermeture window.onload


function verifSelect(){
if($( "#selection" ).val() == ""){
    selection.style.borderColor = "red";
    selectOk = false;
}else{
    selection.style.borderColor = "green";
    selectOk = true;
}
}
function verifRaison (){
   
   if(this.value.length >= 15){
       this.style.borderColor = "green";
       raisonOk = true;
   }else{
       this.style.borderColor = "red";
       raisonOk = false;
   }
}

function sendForm (){
    if (raisonOk == true && selectOk == true)
    {
        $("#form").hide();
        $( "#preForm" ).append( "<h2>Félicitation, votre message a bien été envoyé</h2>" );
    }    
}
